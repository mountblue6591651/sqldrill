USE mountblue;
CREATE TABLE IF NOT EXISTS Patient(
    Patient_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(30),
    DOB DATE,
    Address VARCHAR(30)
);
CREATE TABLE IF NOT EXISTS Doctor(
    Doctor_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    Doctor VARCHAR(30),
    Secretary VARCHAR(30)
);
CREATE TABLE IF NOT EXISTS Prescription(
    Drug VARCHAR(30),
    Date DATE,
    Dosage VARCHAR(30),
    Doctor_id INTEGER,
    FOREIGN KEY (Doctor_id) REFERENCES Doctor(Doctor_id)
);