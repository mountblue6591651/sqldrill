USE mountblue;
CREATE TABLE IF NOT EXISTS Doctor(
    Doctor_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    DoctorName VARCHAR(30),
    Secretary VARCHAR(30)
);
CREATE TABLE IF NOT EXISTS Patient(
    Patient_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    PatientName VARCHAR(30),
    PatientDOB DATE,
    PatientAddress VARCHAR(30)
);
CREATE TABLE IF NOT EXISTS Prescription(
    Drug VARCHAR(30),
    Date DATE,
    Dosage VARCHAR(30),
    Patient_id INTEGER,
    Doctor_id INTEGER,
    FOREIGN KEY (Patient_id) REFERENCES Patient(Patient_id),
    FOREIGN KEY (Doctor_id) REFERENCES Doctor(Doctor_id)
);