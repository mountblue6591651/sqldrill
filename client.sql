USE mountblue;

CREATE TABLE IF NOT EXISTS Location (
    Location_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    Location_name VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS Client (
    Client_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(30),
    Location_id INTEGER,
    FOREIGN KEY (Location_id) REFERENCES Location(Location_id)
);

CREATE TABLE IF NOT EXISTS Manager (
    Manager_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    Manager_Name VARCHAR(30),
    Manager_location_id INTEGER,
    FOREIGN KEY (Manager_location_id) REFERENCES Location(Location_id)
);

CREATE TABLE IF NOT EXISTS Contract (
    Contract_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    Estimated_cost INTEGER,
    Completion_date DATE,
);

CREATE TABLE IF NOT EXISTS Staff (
    Staff_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    Staff_name VARCHAR(30),
    Staff_location_id INTEGER,
    FOREIGN KEY (Staff_location_id) REFERENCES Location(Location_id)
);
