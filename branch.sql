USE mountblue;

CREATE TABLE IF NOT EXISTS Branch (
    Branch_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    Branch_Addr VARCHAR(50),
    Postal_Code INTEGER
);

CREATE TABLE IF NOT EXISTS Book (
    Book_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    ISBN INTEGER,
    Title VARCHAR(50),
    Author VARCHAR(30),
    Publisher VARCHAR(20),
    Num_copies INTEGER
);

CREATE TABLE IF NOT EXISTS LibraryInfo (
    Id INTEGER PRIMARY KEY AUTO_INCREMENT,
    Branch_id INTEGER,
    Book_id INTEGER,
    FOREIGN KEY (Branch_id) REFERENCES Branch(Branch_id),
    FOREIGN KEY (Book_id) REFERENCES Book(Book_id)
);
